package main

import (
	"fmt"

	_ "go.mongodb.org/mongo-driver/mongo"
)

func main() {
	fmt.Println("\U0001F338\U0001F338\U0001F338 From Go Action Demo Pre \U0001F338\U0001F338\U0001F338")
}
