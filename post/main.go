package main

import (
	"fmt"

	_ "github.com/garyburd/redigo/redis"
)

func main() {
	fmt.Println("\U0001F338\U0001F338\U0001F338 From Go Action Demo Post \U0001F338\U0001F338\U0001F338")
}
