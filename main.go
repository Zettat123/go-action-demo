package main

import (
	"fmt"
	"os"

	gha "github.com/sethvargo/go-githubactions"
)

func main() {
	fmt.Println("\U0001F338\U0001F338\U0001F338 Go Action Demo Main, print envs \U0001F338\U0001F338\U0001F338")
	for _, env := range os.Environ() {
		fmt.Println(env)
	}

	name := gha.GetInput("name")
	greetString := fmt.Sprintf("Hello, %s!\n", name)
	gha.SetOutput("greet-string", greetString)
}
